package main

import (
	"io"
	"log"
	"net/http"
	"os"
)

func main() {

	port := os.Getenv("PORT")

	helloHandler := func(w http.ResponseWriter, req *http.Request) {
		io.WriteString(w, "Hello gophers from gitlab CI/CD!")
	}

	http.HandleFunc("/", helloHandler)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

curl -X PATCH https://api.heroku.com/apps/hello-gophers/formation \
--header "Content-Type: application/json" \
--header "Accept: application/vnd.heroku+json; version=3.docker-releases" \
--header "Authorization: Bearer ${HEROKU_PRODUCTION_API_KEY}" \
--data '{ "updates": [ { "type": "web", "docker_image": "'$(cat imageid.txt)'" } ] }'